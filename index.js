var express = require('express');
var config = require('./config/config');
var bodyParser = require('body-parser');

var app = express();
var port = process.env.PORT || 3000;

// use body parser so we can get data from POST and/or url params
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

// load our routes
require('./routes/routes')(express, app);

// kickoff the express app
app.listen(port);
console.log('listening on port: ' + port);
