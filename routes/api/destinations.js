module.exports = function(express, app) {
    var router = express.Router();

    router.get('/destinations', function(req, res, next) {
        var authData = JSON.parse(req.decoded);

        var sampleResult = {
            success:true,
            api:'destinations',
            response:authData
        };

        res.json(sampleResult);
    });

    app.use('/api', router);
};
