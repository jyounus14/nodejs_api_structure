module.exports = function(express, app) {
    var router = express.Router();

    router.get('/update', function(req, res, next) {
        var authData = JSON.parse(req.decoded);

        var sampleResult = {
            success:true,
            api:'update',
            response:authData
        };

        res.json(sampleResult);
    });

    app.use('/', router);
};
