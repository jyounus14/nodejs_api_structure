module.exports = function(express, app) {
    var router = express.Router();

    router.get('/purchaseExtras', function(req, res, next) {
        var authData = JSON.parse(req.decoded);

        var sampleResult = {
            success:true,
            api:'purchaseExtras',
            response:authData
        };

        res.json(sampleResult);
    });

    app.use('/api', router);
};
