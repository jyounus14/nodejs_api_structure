var config = require('../../config/config');
var jwt = require('jsonwebtoken');

module.exports = function(express, app) {
    var router = express.Router();

    router.post('/authenticate', function(req, res, next) {

        var user = {
            name:"Junaid",
            age:24
        };

        var token = jwt.sign(user, config.secret, {
            expiresInMinutes:1440 // expires in 24 hours
        });

        // return the information including token as a JSON
        res.json({
            success:true,
            message:'Enjoy your token!',
            token:token
        });

    });

    app.use('/api', router);
};
