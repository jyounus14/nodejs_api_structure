module.exports = function(express, app) {
    var router = express.Router();

    router.get('/tripNotes', function(req, res, next) {
        var authData = JSON.parse(req.decoded);

        var sampleResult = {
            success:true,
            api:'tripNotes',
            response:authData
        };

        res.json(sampleResult);
    });

    app.use('/api', router);
};
