module.exports = function(express, app) {
    var router = express.Router();

    router.get('/reminders', function(req, res, next) {
        var authData = JSON.parse(req.decoded);

        var sampleResult = {
            success:true,
            api:'reminders',
            response:authData
        };

        res.json(sampleResult);
    });

    app.use('/api', router);
};
