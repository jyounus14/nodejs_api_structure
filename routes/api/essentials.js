module.exports = function(express, app) {
    var router = express.Router();

    router.get('/essentials', function(req, res, next) {
        var authData = JSON.parse(req.decoded);

        var sampleResult = {
            success:true,
            api:'essentials',
            response:authData
        };

        res.json(sampleResult);
    });

    app.use('/api', router);
};
