module.exports = function(express, app) {
    var router = express.Router();

    router.get('/bookings', function(req, res, next) {
        var authData = JSON.parse(req.decoded);

        var sampleResult = {
            success:true,
            api:'bookings',
            response:authData
        };

        res.json(sampleResult);
    });

    app.use('/api', router);
};
