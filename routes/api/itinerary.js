module.exports = function(express, app) {
    var router = express.Router();

    router.get('/itinerary', function(req, res, next) {
        var authData = JSON.parse(req.decoded);

        var sampleResult = {
            success:true,
            api:'itinerary',
            response:authData
        };

        res.json(sampleResult);
    });

    app.use('/api', router);
};
