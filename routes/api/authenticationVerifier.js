var config = require('../../config/config');
var jwt = require('jsonwebtoken');

module.exports = function(express, app) {
    var router = express.Router();

    router.use(function(req, res, next) {
    	// check JSONWebToken
    	var token = req.body.token || req.query.token || req.headers['x-access-token'];
    	var tokenSecret = config.secret;

    	// decode token
    	if(token) {
    		jwt.verify(token, tokenSecret, function(error, decoded) {
    			if(error) {
    				return res.json({ success: false, message: 'Failed to authenticate token.'});

    			} else {
    				req.decoded = JSON.stringify(decoded); // stringify our surname & password values obtained from x-access-token
    				next();
    			}
    		});
            
    	} else {
    		return res.status(403).send({
    			success: false,
    			message: 'No token provided'
    		});
    	}
    });

    app.use('/', router);
};
