module.exports = function(express, app) {
    // just a basic screen when you go to http://localhost:3000/
    var router = express.Router();
    router.get('/', function(req, res, next) {
        res.send('api up and running!');
    });

    app.use('/', router);
    

    // if there's an error, just print it to the console for now
    app.use(function(error, req, res, next) {
        console.error(error.stack);
        res.status(500).send('Something broke.. :/');
    });

    // start loading all the routes
    require('./api/authenticate')(express, app);

    // any routes after the following one will require a valid token to be passed as well
    require('./api/authenticationVerifier')(express, app);

    require('./api/bookings')(express, app);
    require('./api/destination')(express, app);
    require('./api/destinations')(express, app);
    require('./api/essentials')(express, app);
    require('./api/faq')(express, app);
    require('./api/itinerary')(express, app);
    require('./api/paxlist')(express, app);
    require('./api/profile')(express, app);
    require('./api/purchaseExtras')(express, app);
    require('./api/reminders')(express, app);
    require('./api/topPicks')(express, app);
    require('./api/tripNotes')(express, app);
    require('./api/update')(express, app);
};
